// Create a new object, that prototypically inherits from the Error constructor
exports.CustomError = function (name, message) {
    this.name = name;
    this.message = message || 'Default Message';
    this.stack = (new Error()).stack;
};