var fs = require('fs');
var _ = require("underscore");
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var method = FileAssignment.prototype;


function FileAssignment() {
    ///for file path
    this.filePath = "";
    ///column name defination, in future if we wana some do extra thing with some property wee can define here i.e we can write call back function for every property as per requirment
    this.configColumnName = { "Project": {}, "Description": {}, "Start date": { regExp: '^([1-2][0-9]{3})-(0?[1-9]|1[012])-(0?[1-9]|[1-2][0-9]|3[0-1])(\\s)([0-5][0-9])((:?:[0-5][0-9]){2}).([0-9]{3})$' }, "Category": {}, "Responsible": {}, "Savings amount": { regExp: '^(?:|\\d+(\\.\\d{6}))$' }, "Currency": { regExp: "(^[a-zA-Z]+(\s*[a-zA-Z]+)*$)|(^$)" }, "Complexity": { regExp: '((Simple)|(Moderate)|(Hazardous))$' } };
    ////for file column name 
    this.existingColumnObject = {};
    /////after perform operation save data in it;
    this.fileRows = [];
    ////////
    this.rowsData = [];
    ////save the valid file row data
    this.dataArray = [];
}

// main function for showing menu
method.startMainScreen = function () {
    var obj = this;
    try {
        obj.showMain();
        rl.question('Your Command : ', (comm) => {
            // taking input
            console.log("");
            comm = comm.trim().split(" ", 2);
            obj.checkMenu(comm);
        });
    } catch (e) {
        obj.exceptionCatchFn(e);
    }
};

// showiing menu and perform action according to command
method.showMain = function () {
    console.log("\n" +
        "----------------------------------------------------------\n" +
        "-File <path>             full path to the input file \n" +
        "-SortByStartDate         sort results by column Start date in ascending order \n" +
        "-Project <project id>    filter results by column Project \n" +
        "-Exit \n" +
        "----------------------------------------------------------" +
        "\n"
    );
};

method.checkMenu = function (input) {
    var obj = this;
    var fn;

    if (input && _.isArray(input)) {
        var paramConditionCmd = {////SortByStartDate File Exit Project
            'File': function () {
                return obj.readFileCommand(input[1]);
            },
            'SortByStartDate': function () {
                return obj.sortDataByProperty("Start date");
            },
            'Project': function () {
                return obj.filterDataByProperty("Project", input[1]);
            },
            'Exit': function () {
                return process.exit();
            },
            'default': function () {
                return process.exit();
            }
        };
        // if the paramConditionCmd Object contains the value
        // passed in, let's use it
        if (paramConditionCmd[input[0]]) {
            fn = paramConditionCmd[input[0]];
        } else {
            // otherwise we'll assign the default
            fn = paramConditionCmd['default'];
        }
        fn();
        return true;
    } else {
        throw Error('Command parameters are not valid');
    }
};

method.readFileCommand = function (filepath) {
    var obj = this;
    if (obj.readFileData(filepath)) {
        obj.convertFileRowsInArray(obj.fileRows);
        obj.printFullData(obj.existingColumnObject, obj.rowsData);
        obj.startMainScreen();
    } else {
        throw Error("File is not accessable or readable");
    }
};
///pass the file path and read data
method.readFileData = function (filepath) {
    this.filePath = filepath.toString().trim();
    var obj = this;
    //becuase its one file so reading syncnoriously
    //fs.F_OK also check the file permission 
    fs.accessSync(obj.filePath, fs.F_OK);
    obj.fileRows = fs.readFileSync(obj.filePath, 'utf8').toString().split("\n");
    return (obj.fileRows.length > 0 ? true : false);
};
///convert the file data in array form
method.convertFileRowsInArray = function (fileRows) {
    var obj = this;

    //itteration on file rows
    _.each(fileRows, function (val, key) {
        if (obj.check_invalid_line(val))
            return;
        if (_.isEmpty(obj.existingColumnObject)) {
            if (!obj.makeDynamicColumnObjectKey(val)) {
                // throw a error column header
                throw Error("Header Column do not exist in the file");
            }
        } else {
            if (!obj.makeFileRowData(val)) {
                // throw a error column header
                throw Error( "File row data is not valid");

            }
        }
    });
    ///dataArray use for keep orignal data
    obj.dataArray = obj.rowsData;

};
///convert the file data in array form
method.makeFileRowData = function (fileRow) {
    var obj = this;
    ////parse the row data in Array

    var fileRowData = obj.parseRowData(fileRow);
    var rowObj = {};
    _.each(fileRowData, function (val, key) {
        if (obj.existingColumnObject[key] !== undefined) {
            if (obj.checkDataValidation(obj.existingColumnObject[key], val)) {
                rowObj[key] = val;
            } else {
                throw Error(obj.existingColumnObject[key] + " = " + val + " value is not in valid format");
            }
        }
    });
    ///if rowObj is not empty than push in array
    if (!_.isEmpty(rowObj)) {
        //check the row data is equal to column name
        if (Object.keys(obj.existingColumnObject).length != Object.keys(rowObj).length) {
            throw Error("Invalid Row Data", rowObj);
        }
        obj.rowsData.push(rowObj);
    }
    return true;
};
///check the data validation default check the value should not empty
method.checkDataValidation = function (columnName, columnValue) {
    var columnConfig = this.configColumnName[columnName];
    var regExp;
    if (columnConfig.hasOwnProperty('regExp')) {
        regExp = new RegExp(columnConfig.regExp, 'g');
    } else {
        //regular expression for checking not empty value
        regExp = new RegExp('^\\s*\\S.*', 'g');
    }
    return regExp.test(columnValue.toString().trim());
}
//check the line is valid or not
method.check_invalid_line = function (line) {
    return (line.toString().trim() == "" || line[0] == "#");
};
///make a object for column,read the column from file
method.makeDynamicColumnObjectKey = function (topLine) {
    var obj = this;
    var rowData = obj.parseRowData(topLine);
    _.each(rowData, function (val, key) {
        if (obj.configColumnName[val] !== undefined) {
            obj.existingColumnObject[key] = val;
        }
    });
    return Object.keys(obj.existingColumnObject).length;;
};
///parse the row data by tab key
method.parseRowData = function (row) {
    return row.toString().trim().replace(/NULL/g, " ").split("\t");
}
///print the data with header and row
method.printFullData = function (headerName, rowsDataPrint) {
    var obj = this;
    this.printRowData(headerName);
    _.each(rowsDataPrint, function (val, key) {
        obj.printRowData(val);
    });
};
///print the single row data
method.printRowData = function (printRowObj) {
    var showRow = "";
    _.each(printRowObj, function (val, key) {
        showRow = showRow + val + " ";
    });
    console.log(showRow);
};
///print the single row data
method.exceptionCatchFn = function (e) {
    //show error message
    console.log(e.name, e.message); // 'custom message'
    console.log(e.stack);
    process.exit();
};
//////sort the data by any property which you pass
method.sortDataByProperty = function (prop) {
    //first check the property name exist in file
    var obj = this;
    var propIndex = obj.getColumnIndex(prop);
    try {
        obj.rowsData = _.sortBy(obj.dataArray, propIndex);
        obj.printFullData(obj.existingColumnObject, obj.rowsData);
        obj.startMainScreen();
    } catch (e) {
        obj.exceptionCatchFn(e);
    }
};
/////first check the column name exist or return its index value
method.getColumnIndex = function (prop) {
    var obj = this;

    //before sort file must be select
    if (obj.filePath == "") {
        throw Error(prop + " Please select file first");
    }
    var invertHeaderObj = _.invert(obj.existingColumnObject);
    var propIndex = "";
    ///before sorting start date column must be in txt file
    if (invertHeaderObj[prop])
        propIndex = invertHeaderObj[prop];
    else
        throw Error(prop +" column does not exist in file");
    return propIndex;

};
//////sor the data by any property which you pass
method.filterDataByProperty = function (prop, dataId) {
    var obj = this;
    //first check the property name exist in file
    var propIndex = obj.getColumnIndex(prop);

    obj.rowsData = _.filter(obj.dataArray, function (item) {
        if (item[propIndex] == dataId) {
            return item;
        }
    });
    obj.printFullData(obj.existingColumnObject, obj.rowsData);
    obj.startMainScreen();
};
module.exports.FileAssignment = FileAssignment;