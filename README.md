# README #
#Programming assignment#
======================
Write a console application which reads some input data, transforms the data according to the following instructions and finally outputs the results to console.
The application should support the following command line arguments:
#Command (Case sensitive)
#first put the a.txt file on destination drive or folder
* for open file command is
*	File D:/a.txt <File path>	(For file open i.e. file place on D drive)
*	SortByStartDate				(For sort data by startdate column if available)
* 	Project 4 <Project  id> 	(For filter data by project id)

### Install ###
* Install [NodeJS](https://nodejs.org)
* Install libraries: `npm install`
* Run: `node index.js`
#I am using underscore library for my project for some operation
#Here in this project I am supposing some following points
* Dates (Start date) and money (Savings amount) values conform to certain format presented below.Rest of the value must be non empty
* Money format must have the six decimal point
	* 0.123456 valid
	* 1234.458866 valid
	* 1125.4555 invalid
	* 1225.4555455 invalid //because decimal point is 7
* I have made check sorting will work if the sorting column is exist in file
* I have made check filter data by project id if the project column is exist in file
### Run Test Cases ###
*npm test
Test cases work in progress