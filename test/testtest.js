var assert = require('assert');
var expect = require("chai").expect;
var FileAssignment = require("../fileAssignmentClass");
var sinon = require('sinon');
var fileObj;
var filePath = 'd:/a.txt';

describe("checkMenu Test ", function () {
    before(function () {
        fileObj = new FileAssignment.FileAssignment();
    });
    it("check menu throw error on parameter", function () {
        expect(fileObj.checkMenu, 'jkjj').to.throw(Error, /Command parameters are not valid/);
    });
    it("check menu parameter true", function () {
        expect(fileObj.checkMenu(['File', filePath])).to.be.true;
    });
});

describe("readFileData Test", function () {
    before(function () {
        fileObj = new FileAssignment.FileAssignment();
    });
    it("readFileData throw error on parameter", function () {
        expect(fileObj.readFileData, 'jkjj.txt').to.throw(Error);
    });

    it("readFileData return true", function () {
        expect(fileObj.readFileData(filePath)).to.be.true;
    });
});

describe("check_invalid_line Test", function () {
    before(function () {
        fileObj = new FileAssignment.FileAssignment();
    });
    it("check_invalid_line invalid line with #", function () {
        var strLine = "#invalid line";
         expect(fileObj.check_invalid_line(strLine)).to.be.true;
    });

    it("check_invalid_line invalid line with empty", function () {
        var strLine = "";
        expect(fileObj.check_invalid_line(strLine)).to.be.true;
    });
    it("check_invalid_line invalid line with multiple space", function () {
        var strLine = "    ";
        expect(fileObj.check_invalid_line(strLine)).to.be.true;
    });
    it("check_invalid_line valid line with data", function () {
        var strLine = " its a valid line ";
        expect(fileObj.check_invalid_line(strLine)).to.be.false;
    });
});



